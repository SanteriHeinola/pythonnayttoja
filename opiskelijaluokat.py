class Opintojakso():
	def __init__(self, opettajat, aineet, opintop):
		self.opettajat = opettajat
		self.aineet = aineet
		self.opintop = opintop
		

class Oppilas():
	def __init__(self, nimi, ika, kaupunki, koulu, opintojakso):
		self.nimi = nimi.capitalize()
		self.ika = ika
		self.kaupunki = kaupunki.capitalize()
		self.koulu = koulu.capitalize()
		self.opintojaksot = []
		
	def lisaa_jakso(self, jakso):
		self.opintojaksot.append(jakso)

	def hae_jaksot(self):
		return self.opintojaksot
	
	def opintop_yhteensa(self):
		yhteensa = 0
		for opintojakso in self.hae_jaksot():
			yhteensa += opintojakso.opintop
		return yhteensa
		
def main():#ajetaan aliohjelmat
	oppilas1 = Oppilas(input("Anna nimi: "), input("Anna ika: "), input("Anna kaupunki: "), input("Anna koulu: "), input(""))
	
	opintojaksot = {
		'Matikka': Opintojakso("Mikko ja Marja", "Matikka", 21),
		'Ruotsi': Opintojakso("Kalle ja Pirjo", "Ruotsi", 24),
		'Python': Opintojakso("Mikko ja Marja", "Python", 24),
		'Javascript': Opintojakso("Tommi ja Lauri", "Javascript", 23),
		'Wordpress': Opintojakso("Kalle ja Pirjo", "Wordpress", 20)
	}
	
	for opintojakson_nimi in opintojaksot:
		print("Lisataanko opintojakso: k/e?")
		print(opintojaksot[opintojakson_nimi].aineet)
		vastaus = input()
		if vastaus == "k":
			oppilas1.lisaa_jakso(opintojaksot[opintojakson_nimi])

	print("\nOlet " + oppilas1.nimi + "\n\nIka:" + oppilas1.ika + " vuotta\n\nKoti paikkakunta: " + oppilas1.kaupunki + "\n\nOpiskelupaikka: " + oppilas1.koulu + "\n\nOpintojaksosi: ")
	
	for opintojakso in oppilas1.hae_jaksot():
		print("Opintojakso: \n" + opintojakso.aineet + " Jakson opettajat: " + opintojakso.opettajat + " Jakson opintopisteet: " + str(opintojakso.opintop) + " pistettä!\n")
		
	print("Opintopisteitä suoritettavana: " + str(oppilas1.opintop_yhteensa()))
	
main()  #ajetaan main