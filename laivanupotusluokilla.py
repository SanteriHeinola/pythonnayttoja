def laukaus (lista, loppu, osuma, huti, a): #printataan taulukko ja kysytaan koordinaatit
    while True:
        for i in range(0, len(lista)):
           print(lista[i])
        x = input ("Anna rivi= ")
        z = input ("Anna sarake= ")

        if lista[x - 1 ] [z - 1] == 1:#tarkistetaan osuiko vertaamalla x ja z arvoja listaan
            lista[x - 1 ] [z - 1] = 0
            print ("Osuma!")#merkataan huti ylos
            osuma += 1

            if lista == loppu: #kun lista on sama kuin loppu, olkoon a false
                a = True;
                return lista, loppu, osuma, huti, a

            return lista, loppu, osuma, huti, a

        else:
            print ("Huti...")#jos ei osunut, niin merkataan huti ylos
            huti += 1
            return lista, loppu, osuma, huti, a

def tulokset(lista, loppu, osuma, huti):
    if lista == loppu:
        print ("Upotit laivat onnistuneesti!\n"+"Loppupisteet:")
        print ("Osumat=" + str(osuma))
        print ("Hudit=" + str(huti))


def main():
    #annetaan taulukko ja arvo joka paattaa pelin
	#lista = [[1,1,0,0,0],[0,1,1,0,0],[0,0,1,1,0],[0,0,0,1,1],[1,0,0,0,1]]
    lista = [[1,1,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
    loppu = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
    osuma = 0
    huti = 0
    a = False

    while a == False: #loopataan kunnes a on true
        lista, loppu, osuma, huti, a = laukaus (lista, loppu, osuma, huti, a)
        tulokset(lista, loppu, osuma, huti)

main() #main-funktio ajoon