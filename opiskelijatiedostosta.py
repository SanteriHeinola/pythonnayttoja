import pickle
from pathlib import Path

class Opintojakso():
	def __init__(self, opettajat, aineet, opintop):
		self.opettajat = opettajat
		self.aineet = aineet
		self.opintop = opintop
	def __str__(self):
		return "%s %s %s" %(self.opettajat, self.aineet, self.opintop)

class Oppilas():
	def __init__(self, nimi, ika, kaupunki, koulu, opintojakso):
		self.nimi = nimi.capitalize()
		self.ika = ika
		self.kaupunki = kaupunki.capitalize()
		self.koulu = koulu.capitalize()
		self.opintojaksot = []
		
	def lisaa_jakso(self, jakso):
		self.opintojaksot.append(jakso)

	def hae_jaksot(self):
		return self.opintojaksot
	
	def opintop_yhteensa(self):
		yhteensa = 0
		for opintojakso in self.hae_jaksot():
			yhteensa += opintojakso.opintop
		return yhteensa
	def __str__(self):
		return "%s %s %s %s %s" %(self.nimi, self.ika, self.kaupunki, self.koulu, self.opintojaksot)
		#return "nimi %s ika %s kaupunki %s koulu %s opintojaksot %s" (self.nimi, self.ika, self.kaupunki, self.koulu, self.opintojaksot)

def main():#ajetaan aliohjelmat
	#from b import (Opintojakso)
	#from a import (Oppilas)
	
	oppilas1 = Oppilas(input("Anna nimi: "), input("Anna ika: "), input("Anna kaupunki: "), input("Anna koulu: "), input(""))
	tulos=[]
	opintojaksot = {
		'Matikka': Opintojakso("Mikko ja Marja", "Matikka", 21),
		'Ruotsi': Opintojakso("Kalle ja Pirjo", "Ruotsi", 24),
		'Python': Opintojakso("Mikko ja Marja", "Python", 24),
		'Javascript': Opintojakso("Tommi ja Lauri", "Javascript", 23),
		'Wordpress': Opintojakso("Kalle ja Pirjo", "Wordpress", 20)
	}
	
	for opintojakson_nimi in opintojaksot:
		print("Lisataanko opintojakso: k/e?")
		print(opintojaksot[opintojakson_nimi].aineet)
		vastaus = input()
		if vastaus == "k":
			oppilas1.lisaa_jakso(opintojaksot[opintojakson_nimi])

	print("\nOlet " + oppilas1.nimi + "\n\nIka:" + oppilas1.ika + " vuotta\n\nKoti paikkakunta: " + oppilas1.kaupunki + "\n\nOpiskelupaikka: " + oppilas1.koulu + "\n\nOpintojaksosi: ")
	
	for opintojakso in oppilas1.hae_jaksot():
		print("Opintojakso: \n" + opintojakso.aineet + " Jakson opettajat: " + opintojakso.opettajat + " Jakson opintopisteet: " + str(opintojakso.opintop) + " pistettä!\n")
		
	print("Opintopisteitä suoritettavana: " + str(oppilas1.opintop_yhteensa()))
	


	tdstoNimi=Path("opintotiedot.data")

	print(oppilas1.hae_jaksot())
    #Poikkeuskäsittely siksi, että tiedosto voi olla olemassa, mutta se on tyhjä, jolloin load aiheuttaa poikkeuksen
	
	try:
		if tdstoNimi.exists():#Jos tiedosto on olemassa
			#Ei tarvita tiedoston sulkemista erikseen, kun käytetään 'with open...'
			with open(tdstoNimi, "rb") as tdsto:#rb == Read Binary
				tulos=pickle.load(tdsto)#Luetaan tiedoston sisältö muuttujaan tulos
			tulos=tulos+oppilas1.hae_jaksot()#Liitetään syötteenä saadut kolmiot tiedostosta saatuun tietoon
		else:
			tulos=oppilas1.hae_jaksot()#Jos tiedostoa ei ollut olemassa, saa tulos arvokseen näppäimistöltä syötetyt riistakolmiot
	except:#Mikä tahansa poikkeus
		tulos=oppilas1.hae_jaksot()#Myös poikkeustapauksessa juttu

	#Kirjoitetaan tiedostoon aikaisemmin saatu tulos ja siihen liitetyt uudet riistakolmiot
	with open(tdstoNimi, "wb") as tdsto:#wb == Write Binary
		pickle.dump(tulos, tdsto)

	#Luetan tiedostosta - testimielessä - kaikki riistakolmiot
	with open(tdstoNimi, "rb") as tdsto2:
		tulos=pickle.load(tdsto2)
	
	#print("tulos: ", tulos)
	#print(oppilas1)

	for opinnot in tulos:#Käydään toistolauseessa läpi kaikki riistakolmiot
		print("after ", end=" ")
		print(opinnot) #Tulostetaan opinnot nimi - ks. Luokan opinnot metodi __str__(self)
		#for riista in kolmio.riista:#Käydään käsittelyssä olevan riistakolmion Riistalista läpi
			#print("\t",riista)#Tulostetaan riista - ks. Riista-luokan metodi __str__(self)
main()  #ajetaan main