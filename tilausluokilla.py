def menun_teko(menu):
	while True: #hankitaan tuotteiden arvot
		print ("\nLisaa tuote:\n") 
	#lisataan tuote
		nimi = raw_input("Anna ruoan nimi: ").capitalize()
		if nimi != "":#jos syote ei ole tyhja, kysytaan ruoan hintaa
			hinta = input("Anna ruoan hinta: ")
			menu[nimi] = hinta
			print menu
		else: #muuten lopetetaan aliohjelma
			break
	return (menu)

def tilauksen_teko(menu): # Aloitetaan tilauksen teko
	tilaukset = {}
	hinta_yht = 0
	while True: 
		print ("\nTassa ruokalista...")
		for attr, value in  menu.items():# Tulostetaan avain ja arvo
			print (attr, value) 
		tuote = raw_input("\nMita saisi olla?\n").capitalize()#Kysyy tuotteen
		if tuote != "":#jos syote ei ole tyhja, lisataan ruoka tilaukseen ja lasketaan tilauksen yhteisarvo
			tilaukset[tuote] = menu[tuote]# Tulostetaan menu
			hinta_yht += float(tilaukset[tuote])
		else:#muuten lopetetaan aliohjelma
			break
	return (tilaukset, hinta_yht)

def loppusumma(tilaukset, hinta_yht): 
	for attr, value in  tilaukset.items():# Tulostetaan tilauksen avain ja arvo
		print (attr, value)
	hinta_yht = sum(tilaukset.values())
	print "\nTassa laskunne" 
	for value in tilaukset.items(): 
		print str(value)# Tulostetaan tilauksen avain ja arvo
	print ("Loppuhinta: ")
	print hinta_yht#tulostetaan kokonaishinta

def main():#ajetaan aliohjelmat
	menu={}
	menu = menun_teko(menu)
	tilaukset, hinta_yht = tilauksen_teko(menu)
	loppusumma(tilaukset, hinta_yht)
	
main()  # ajetaan main